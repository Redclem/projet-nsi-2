var noms = ["", "Rond","Croix"];
var rond = 1;
var croix = 2;

class Morpion
{
  constructor()
  {
    this.canvas = null;
    this.context = null;
    this.joueur = 0;
    this.grille = null;
    this.temps = null;
    this.infoText = null;
    this.jeu_termine = null;
    this.scores = [0,0];
  }

  resize()
  {
    this.canvas.height = window.innerHeight * 9 / 10;
    if((window.innerWidth * 9 / 10) < this.canvas.height * 4 / 3)
    {
      this.canvas.width = window.innerWidth * 9 / 10;
      this.canvas.height = this.canvas.width * 3 / 4;
    }
    this.canvas.width = this.canvas.height * 4 / 3;
  }

  update_anim()
  {
    for(var iter = 0;iter != 9;iter++)
    {
      if(this.temps[iter] != 0 && this.temps[iter] != 50)
      {
        this.temps[iter]++;
      }
    }
    this.render();
  }

  render()
  {
    this.context.clearRect(0,0,this.canvas.width, this.canvas.height);

    this.context.lineWidth = 3;

    this.context.fillStyle = "#7F0000";
    this.context.fillRect(0,0,this.canvas.width * 3 / 4,this.canvas.height);

    this.context.fillStyle = "#FFFF00";
    this.context.fillRect(this.canvas.width * 3 / 4, 0, this.canvas.width / 3, this.canvas.height / 3);

    this.context.fillStyle = "#FFFFFF";
    this.context.fillRect(this.canvas.width * 3 / 4, this.canvas.height / 3, this.canvas.width / 4, this.canvas.height * 2 / 3);

    this.context.fillStyle = "#000000";

    this.context.font = (this.canvas.width / 40).toString() + "px sans-serif";
    this.context.textAlign = "center";
    this.context.fillText("Recommencer",this.canvas.width * 7 / 8 ,this.canvas.height / 6);

    this.context.font = (this.canvas.width / 60).toString() + "px sans-serif";
    this.context.textAlign = "center";
    this.context.fillText(this.infoText,this.canvas.width * 7 / 8 ,this.canvas.height * 5 / 6);

    this.context.fillText("Score croix: " + this.scores[croix - 1],this.canvas.width * 7 / 8 ,this.canvas.height * 5 / 12);

    this.context.fillText("Score rond: " + this.scores[rond - 1],this.canvas.width * 7 / 8 ,this.canvas.height * 7 / 12);




    for(var num_ligne = 1;num_ligne != 3;num_ligne++) // dessin grille
    {
      this.context.beginPath();
      this.context.moveTo(this.canvas.width * num_ligne / 4,0);
      this.context.lineTo(this.canvas.width * num_ligne / 4,this.canvas.height);
      this.context.stroke();

      this.context.beginPath();
      this.context.moveTo(0,this.canvas.height * num_ligne / 3);
      this.context.lineTo(this.canvas.width,this.canvas.height * num_ligne / 3);
      this.context.stroke();
    }

    this.context.beginPath(); // dessin ligne de separation a droite
    this.context.moveTo(this.canvas.width * 3 / 4,0);
    this.context.lineTo(this.canvas.width * 3 / 4,this.canvas.height);
    this.context.stroke();

    this.context.beginPath(); // dessin ligne de separation des scores
    this.context.moveTo(this.canvas.width * 3 / 4,this.canvas.height/2);
    this.context.lineTo(this.canvas.width,this.canvas.height/2);
    this.context.stroke();

    for(var iter = 0;iter != 9;iter++)
    {
      var x = iter % 3;
      var y = Math.floor(iter / 3);
      if(this.grille[iter] == rond)
      {
        this.context.beginPath();
        this.context.arc(x * this.canvas.width / 4 + this.canvas.width / 8,y * this.canvas.height / 3 + this.canvas.height / 6,(this.canvas.height / 6 - 5) * this.temps[iter] / 50,0,2 * Math.PI);
        this.context.closePath();
        this.context.stroke();
      }
      else if (this.grille[iter] == croix) {
        this.context.beginPath();
        this.context.moveTo((x + 1 - (this.temps[iter] + 55) / 105)* this.canvas.width / 4 + 5, (y + 1 - (this.temps[iter] + 55) / 105) * this.canvas.height / 3 + 5);
        this.context.lineTo((x + (this.temps[iter] + 55) / 105) * this.canvas.width / 4 - 5, (y + (this.temps[iter] + 55) / 105) * this.canvas.height / 3 - 5);
        this.context.stroke();

        this.context.beginPath();
        this.context.moveTo((x + 1 - (this.temps[iter] + 55) / 105) * this.canvas.width / 4 + 5, (y + (this.temps[iter] + 55) / 105) * this.canvas.height / 3 - 5);
        this.context.lineTo((x + (this.temps[iter] + 55) / 105) * this.canvas.width / 4 - 5, (y + 1 - (this.temps[iter] + 55) / 105) * this.canvas.height / 3 + 5);
        this.context.stroke();
      }
    }
  }

  reset()
  {
    this.joueur = Math.floor(Math.random() * 2) + 1;
    this.grille = [
      0,0,0,
      0,0,0,
      0,0,0
    ];
    this.temps = [
      0,0,0,
      0,0,0,
      0,0,0
    ];
    this.infoText = noms[this.joueur] + " joue";
    this.jeu_termine = false;

  }

  init()
  {
    this.canvas = document.getElementById("canvas");
    if(this.canvas  == null)
    {
      alert("Canvas non trouvé");
      return;
    }

    this.resize();

    this.context = this.canvas.getContext("2d");
    if(this.context == null)
    {
      alert("Contexte non supporté");
      return;
    }

    this.reset();
  }

  onClick(ev)
  {
    var canv_rect = this.canvas.getBoundingClientRect();
    var x = Math.floor((ev.clientX - canv_rect.left) * 4 /this.canvas.width);
    var y = Math.floor((ev.clientY - canv_rect.top) * 3 / this.canvas.height);

    if(x < 3 && !this.jeu_termine)
    {
      if(this.grille[x + 3 * y] != 0)
      {
        this.infoText="Cette case est occupée";
      }
      else
      {
        this.grille[x + 3 * y] = this.joueur;
        this.temps[x + 3 * y] = 1;
        var gagnant = this.testVictoire();
        if(gagnant != 0)
        {
          this.jeu_termine = true;
          this.infoText = noms[this.joueur] + " a gagné";
          this.scores[this.joueur - 1]++;

        }
        else if(!this.grille.includes(0))
        {
          this.jeu_termine = true;
          this.infoText = "Egalité";
        }
        else {
          this.joueur = 3 - this.joueur;
          this.infoText = noms[this.joueur] + " joue";
        }

      }
    }


    if(x == 3 && y == 0)
    {
      this.reset();
    }
  }

  testVictoire()
  {
    for(var x = 0;x != 3;x++)
    {
      if(this.grille[3 * x] == this.grille[3 * x + 1] && this.grille[3 * x + 1] == this.grille[3 * x + 2] && this.grille[3 * x] != 0)
      {
        return this.grille[3 * x];
      }

      if(this.grille[x] == this.grille[3+x] && this.grille[3 + x] == this.grille[6 + x] && this.grille[x] != 0)
      {
        return this.grille[x];
      }
    }
    if(this.grille[0] == this.grille[4] && this.grille[4] == this.grille[8] && this.grille[0] != 0)
    {
      return this.grille[0];
    }
    if(this.grille[2] == this.grille[4] && this.grille[4] == this.grille[6] && this.grille[2] != 0)
    {
      return this.grille[2];
    }
    return 0;
  }
}

function main()
{
  morp = new Morpion;
  morp.init();

  window.addEventListener("resize", function() {morp.resize();});
  morp.canvas.addEventListener("click", function(ev) {morp.onClick(ev);});
  setInterval(function() {morp.update_anim();},10);
}

main();
