
class Main {
  constructor()
  {
    this.canvas = null;
    this.gl = null;
    this.shader = null;
    this.vertex_buffer = null;
    this.index_buffer = null;
    this.positions = {
      vertex : null,
      matrix : null
    };
    this.initialized = false;
    this.angle = 0;
  }

  resizeCanvas()
  {

    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    if(this.gl)
    {
      this.gl.viewport(0, 0, this.canvas.width, this.canvas.height);
    }

    if(this.initialized)
    {
      this.render();
    }
  }

  get_canvas_and_context()
  {
    this.canvas = document.getElementById("canvas");
    this.gl = canvas.getContext("webgl");
  }

  clear_canvas()
  {
    this.gl.clearColor(0.0,0.0,0.0,0.0);
    this.gl.clearDepth(1.0);
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
  }

  create_shaders()
  {
    var vert_shader_s = `
      attribute vec3 pos;
      uniform mat4 mat;

      varying mediump vec3 col;

      void main()
      {
        gl_Position = vec4(pos,1.0) * mat;
        col = pos * 0.5 + vec3(0.5,0.5,0.5);
      }
    `;

    var frag_shader_s = `
      varying mediump vec3 col;

      void main()
      {
        gl_FragColor = vec4(col,1.0);
      }
    `;

    var vertex_shader = this.gl.createShader(this.gl.VERTEX_SHADER);
    this.gl.shaderSource(vertex_shader,vert_shader_s);
    this.gl.compileShader(vertex_shader);

    if(!this.gl.getShaderParameter(vertex_shader,this.gl.COMPILE_STATUS))
    {

      alert("Echec compilation vertex shader " + this.gl.getShaderInfoLog(vertex_shader));
    }

    var frag_shader = this.gl.createShader(this.gl.FRAGMENT_SHADER);
    this.gl.shaderSource(frag_shader,frag_shader_s);
    this.gl.compileShader(frag_shader);

    if(!this.gl.getShaderParameter(frag_shader,this.gl.COMPILE_STATUS))
    {
      alert("Echec compilation fragment shader " + this.gl.getShaderInfoLog(frag_shader));
    }

    this.shader = this.gl.createProgram();

    this.gl.attachShader(this.shader, vertex_shader);
    this.gl.attachShader(this.shader, frag_shader);

    this.gl.linkProgram(this.shader);

    if(!this.gl.getProgramParameter(this.shader,this.gl.LINK_STATUS))
    {
      alert("Erreur link shader : " + this.gl.getProgramInfoLog(this.shader));
    }

    this.positions.vertex = this.gl.getAttribLocation(this.shader,"pos");
    this.positions.matrix = this.gl.getUniformLocation(this.shader,"mat");
  }

  initialize()
  {
    this.get_canvas_and_context();
    this.resizeCanvas();

    if(this.gl == null)
    {
      alert("Webgl non supporté!");
      return;
    }

    this.clear_canvas();

    this.create_shaders();
    if(this.shader == null)
    {
      alert("Erreur compilation shaders");
      return;
    }

    this.createVertexBuffer();

    if(this.vertex_buffer == null)
    {
      alert("Erreur creation vertex buffer");
      return;
    }

    this.createIndexBuffer();

    if(this.index_buffer == null)
    {
      alert("Erreur creation index shader");
      return;
    }

    this.initialized = true;
  }

  createVertexBuffer()
  {
    this.vertex_buffer = this.gl.createBuffer();
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertex_buffer);
    const positions = [
      -1.0,-1.0,-1.0,
      1.0,-1.0,-1.0,
      -1.0,-1.0,1.0,
      1.0,-1.0,1.0,
      -1.0,1.0,-1.0,
      1.0,1.0,-1.0,
      -1.0,1.0,1.0,
      1.0,1.0,1.0
    ];

    this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(positions),this.gl.STATIC_DRAW);
  }

  createIndexBuffer()
  {
    this.index_buffer = this.gl.createBuffer();
    this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.index_buffer);
    const indexes = [
      0,1,2, 2,1,3,
      2,3,6, 6,3,7,
      1,5,3, 3,5,7,
      0,4,1, 1,4,5,
      4,0,2, 4,2,6,
      4,6,5, 5,6,7
    ];

    this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexes),this.gl.STATIC_DRAW);
  }

  render()
  {
    if(!this.initialized)
    {
      return;
    }
    this.gl.clearColor(0.0,0.0,0.0,0.0);
    this.gl.clearDepth(1.0);
    this.gl.enable(this.gl.DEPTH_TEST);
    this.gl.depthFunc(this.gl.LEQUAL);

    this.gl.enable(this.gl.CULL_FACE);
    this.gl.cullFace(this.gl.BACK);

    this.gl.clear(this.gl.COLOR_BUFFER_BIT,this.gl.DEPTH_BUFFER_BIT);

    this.gl.bindBuffer(this.gl.ARRAY_BUFFER,this.vertex_buffer);
    this.gl.vertexAttribPointer(this.positions.vertex, 3,this.gl.FLOAT,false,0,0);
    this.gl.enableVertexAttribArray(this.positions.vert_pos);

    this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER,this.index_buffer);

    this.gl.useProgram(this.shader);

    const mat = mat4.identity().rotate_z(this.angle).translate(0.0,3.0,0.0).z_up().proj(this.canvas.width / this.canvas.height);

    this.gl.uniformMatrix4fv(this.positions.matrix, false, mat.data);

    this.gl.drawElements(this.gl.TRIANGLES,36,this.gl.UNSIGNED_SHORT,0);
  }

  render_and_increment()
  {
    this.render();
    this.angle = (this.angle + Math.PI / 100) % (Math.PI * 2);
  }
}

function boucle_rendu()
{
  main_object.render_and_increment();
  window.setTimeout(boucle_rendu,1000/50);
}

function main()
{
  main_object = new Main;

  main_object.initialize();
  window.addEventListener("resize",function() {main_object.resizeCanvas();});
  window.setTimeout(boucle_rendu,2000);
}

window.onload = main;
