class mat4{
  constructor()
  {
    this.data = new Float32Array(
    [
      0.0,0.0,0.0,0.0,
      0.0,0.0,0.0,0.0,
      0.0,0.0,0.0,0.0,
      0.0,0.0,0.0,0.0
    ]);
  }

  static identity()
  {
    var res = new mat4;
    res.data = new Float32Array(
    [
      1.0,0.0,0.0,0.0,
      0.0,1.0,0.0,0.0,
      0.0,0.0,1.0,0.0,
      0.0,0.0,0.0,1.0
    ]);
    return res;
  }

  translate(x, y, z) {
    var temp = mat4.identity();
    temp.data[3] += x;
    temp.data[7] += y;
    temp.data[11] += z;
    return this.multiply(temp);
  }

  scale(x, y, z) {
    var temp = mat4.identity();
    temp.data[0] *= x;
    temp.data[5] *= y;
    temp.data[10] *= z;
    return this.multiply(temp);
  }

  z_up()
  {
    var temp = mat4.identity();
    temp.data[5] = 0.0;
    temp.data[10] = 0.0;
    temp.data[6] = 1.0;
    temp.data[9] = 1.0;
    return this.multiply(temp);
  }

  multiply(matrix2)
  {
    var res = new mat4;
    for(var iter = 0;iter != 16;iter++)
    {
      res.data[iter] += matrix2.data[(iter >> 2) * 4] * this.data[iter % 4];
      res.data[iter] += matrix2.data[(iter >> 2) * 4 + 1] * this.data[iter % 4 + 4];
      res.data[iter] += matrix2.data[(iter >> 2) * 4 + 2] * this.data[iter % 4 + 8];
      res.data[iter] += matrix2.data[(iter >> 2) * 4 + 3] * this.data[iter % 4 + 12];
    }
    return res;
  }

  proj(aspect_ratio)
  {
    var res = new mat4;
    res.data = new Float32Array([
      (1.0/aspect_ratio),0.0,0.0,0.0,
      0.0,1.0,0.0,0.0,
      0.0,0.0,0.9,-0.1,
      0.0,0.0,1.0,0.0
    ]);
    return this.multiply(res);
  }

  rotate_z(a)
  {
    var res = new mat4;
    res.data = new Float32Array([
      Math.cos(a),Math.sin(a),0.0,0.0,
      -Math.sin(a),Math.cos(a),0.0,0.0,
      0.0,0.0,1.0,0.0,
      0.0,0.0,0.0,1.0
    ]);
    return this.multiply(res);
  }
}
